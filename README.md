
pug
==
**Pug, the Udk Guidedog** that 
1. reduces the code maintenance complexity and
2.  simplifies the build process of UDK.

This is a Python/Pypi package.


## Ref.
- http://otuk.kodeten.com/making-a-python-package-for-pypi---easy-steps/
- [Todo-list](https://hackmd.io/SeYaoagMTkeJEF6LrM6DPw?view)
- Pip-install using github: `python -m pip install --user --upgrade git+https://github.com/timotheuslin/ipug`
